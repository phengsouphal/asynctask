package com.example.gamer.asybctask;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    EditText edvalue;
    TextView txtresult;
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView listView=findViewById(R.id.lstview);

        edvalue=findViewById(R.id.edvalue);
        txtresult=findViewById(R.id.txtshow);

        btn=findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiTask multiTask=new MultiTask();
                String sleepTime=edvalue.getText().toString();
                multiTask.execute(sleepTime);
            }
        });

//        Retrofit retrofit=new Retrofit.Builder()
//                .baseUrl(Api.BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        Api api=retrofit.create(Api.class);
//        Call<List<Hero>> call=api.getHeroes();
//
//        call.enqueue(new Callback<List<Hero>>() {
//            @Override
//            public void onResponse(Call<List<Hero>> call, Response<List<Hero>> response) {
//
//                List<Hero> heroes=response.body();
//              String[]heroNames=new String[heroes.size()];
//                for (int i = 0; i < heroes.size(); i++) {
//                    heroNames[i]=heroes.get(i).getName();
//                }
//
//                listView.setAdapter(new ArrayAdapter<String>(
//                        getApplicationContext(),android.R.layout.simple_list_item_1,
//                        heroNames
//                ));
//
//
//            }
//
//            @Override
//            public void onFailure(Call<List<Hero>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_SHORT).show();
//            }
//        });



    }

    public class MultiTask extends AsyncTask<String,String,String>{
        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... strings) {
          publishProgress("Sleeping...");
          try {
              int time=Integer.parseInt(strings[0])*1000;
              Thread.sleep(time);
              resp="Sleep For "+strings[0]+" Seconds";

          } catch (InterruptedException e) {
              e.printStackTrace();
              resp=e.getMessage();
          }catch (Exception e){
              e.printStackTrace();
              resp=e.getMessage();
          }
            return resp;
        }

        @Override
        protected void onPreExecute() {
         progressDialog=ProgressDialog.show(MainActivity.this,
                 "ProgressDialog",
                 "Wait For "+edvalue.getText().toString()+" Second");
        }

        @Override
        protected void onPostExecute(String s) {
         progressDialog.dismiss();
         txtresult.setText(s);
        }

        @Override
        protected void onProgressUpdate(String... values) {
           txtresult.setText(values[0]);
        }
    }

}
